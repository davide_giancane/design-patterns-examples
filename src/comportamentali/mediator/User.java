package comportamentali.mediator;

/**
 * Gli utenti possono inviare e ricevere messaggi
 */
public abstract class User {
    //ogni utente possiede il riferimento al mediator, che sarà necessario per la comunicazione fra utenti
    protected ChatMediator mediator;
    protected String name;

    public User(ChatMediator mediator, String name){
        this.mediator = mediator;
        this.name = name;
    }

    public abstract void send(String msg);

    public abstract void receive(String msg);
}
