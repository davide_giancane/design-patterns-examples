package comportamentali.mediator;

public class UserImpl extends User {
    public UserImpl(ChatMediator mediator, String name){
        super(mediator, name);
    }

    /**
     * IL metodo usa il mediator per inviare il messaggio agli utenti e non ha idea di come questo venga gestito dal
     * mediator
     * @param msg messaggio da inviare
     */
    @Override
    public void send(String msg) {
        System.out.println(this.name + ": sending message = " + msg);
        mediator.sendMessage(msg, this);
    }

    @Override
    public void receive(String msg) {
        System.out.println(this.name + ": received message = " + msg);
    }
}
