package comportamentali.mediator;

/**
 * Interfaccia per oggetti di tipo Mediator.
 * Un mediator ha il compito di inviare un certo messaggio ad un utente e di aggiungere utenti alla chat room
 */
public interface ChatMediator {
    public void sendMessage(String msg, User user);
    void addUser(User user);
}
