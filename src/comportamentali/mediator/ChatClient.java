package comportamentali.mediator;

/**
 * Testiamo la chat room con un semplice programma dove creamo il mediator, ci aggiungiamo gli utenti del gruppo e un
 * utente invia un proprio messaggio
 */
public class ChatClient {
    public static void main(String[] args) {
        ChatMediator mediator = new ChatMediatorImpl();

        User user1 = new UserImpl(mediator, "Davide");
        User user2 = new UserImpl(mediator, "Sara");
        User user3 = new UserImpl(mediator, "Riccardo");
        User user4 = new UserImpl(mediator, "Stefano");

        mediator.addUser(user1);
        mediator.addUser(user2);
        mediator.addUser(user3);
        mediator.addUser(user4);

        user1.send("Ciao ragazzi!");
    }
}
