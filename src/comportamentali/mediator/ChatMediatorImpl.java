package comportamentali.mediator;

import java.util.ArrayList;
import java.util.List;

public class ChatMediatorImpl implements ChatMediator{
    //il mediator deve possedere una lista di oggetti che deve mettere in comunicazione
    private List<User> users;

    public ChatMediatorImpl(){
        this.users = new ArrayList<>();
    }

    @Override
    public void sendMessage(String msg, User user) {
        //il messaggio non deve essere ricevuto dall'utente che lo sta inviando
        users.stream()
                .filter(u -> u != user)
                .forEach(u -> u.receive(msg));
    }

    @Override
    public void addUser(User user) {
        users.add(user);
    }
}
