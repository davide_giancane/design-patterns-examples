package comportamentali.template_method;

public abstract class Game {

    //Implementazioni specifiche delle sottoclassi
    abstract void initialize();
    abstract void startPlay();
    abstract void endPlay();

    /**
     * TEMPLATE METHOD
     * (dichiarato final per prevenire il suo overriding dalle sottoclassi)
     */
    public final void play(){
        initialize(); //inizializzo il gioco
        startPlay(); //inizio a giocare
        endPlay(); //finisco di giocare
    }
}
