package comportamentali.template_method;

public class TemplateMethodExample {
    public static void main(String[] args) {
        Game football = new Football();
        football.play();

        System.out.println();

        Game cricket = new Cricket();
        cricket.play();
    }
}
