package comportamentali.iterator;

public class NameRepository implements Container {
    public String[] names = {"John", "David", "Sara", "Sofia"};

    @Override
    public Iterator getIterator() {
        return new NameIterator(this);
    }

    public String[] getNames() {
        return names;
    }
}
