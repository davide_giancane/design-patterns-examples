package comportamentali.iterator;

interface Container {
    public Iterator getIterator();
    //....metodi caratteristici del container (aggiungi elemento, rimuovi elemento, ecc...)
}
