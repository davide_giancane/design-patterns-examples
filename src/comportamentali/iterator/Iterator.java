package comportamentali.iterator;

interface Iterator {
    public boolean hasNext();
    public void next();
    public Object first();
    public Object current();
    //..... possibili altri metodi per riferirsi a particolari oggetti della struttura
}
