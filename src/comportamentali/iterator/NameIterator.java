package comportamentali.iterator;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class NameIterator implements Iterator {
    private NameRepository repository;
    int index;

    public NameIterator(NameRepository repository){
        this.repository = repository;
        List<String> list = new ArrayList<>();
        list.iterator();
    }

    @Override
    public boolean hasNext() {
        return (index < repository.getNames().length - 1);
    }

    @Override
    public void next() {
        index++;
    }

    @Override
    public Object first() {
        index = 0;
        return repository.getNames()[index];
    }

    @Override
    public Object current() {
        if(index == repository.getNames().length - 1)
            throw new NoSuchElementException();
        else
            return repository.getNames()[index];
    }
}
