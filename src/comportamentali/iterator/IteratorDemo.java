package comportamentali.iterator;

public class IteratorDemo {
    public static void main(String[] args) {
        Container namesContainer = new NameRepository();
        Iterator namesIterator = namesContainer.getIterator();

        while (namesIterator.hasNext()){
            System.out.println(namesIterator.current());
            namesIterator.next();
        }
    }
}
