package comportamentali.strategy;

public class StrangeFlyStrategy implements FlyStrategy {
    @Override
    public void fly() {
        System.out.println("Sadly I'm flying with just one wing...");
    }
}
