package comportamentali.strategy;

public class StrategyExample {
    public static void main(String[] args) {
        //creo due tipologie di duck impostando nel costruttore i comportamenti desiderati
        Duck firstDuck = new CityDuck(new SimpleQuackStrategy(), new StrangeFlyStrategy());
        Duck secondDuck = new WildDuck(new ComplexQuackStrategy(), new SimpleFlyStrategy());

        firstDuck.fly();
        firstDuck.quack();

        System.out.println();

        secondDuck.fly();
        secondDuck.quack();
    }
}
