package comportamentali.strategy;

public class ComplexQuackStrategy implements QuackStrategy {
    @Override
    public void quack() {
        System.out.println("complex quacking...QUI! QUO! QUAAA!");
    }
}
