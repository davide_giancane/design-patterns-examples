package comportamentali.strategy;

public interface FlyStrategy {
    void fly();
}
