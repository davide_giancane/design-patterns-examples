package comportamentali.strategy;

public class CityDuck extends Duck {

    public CityDuck(QuackStrategy quackStrategy, FlyStrategy flyStrategy){
        this.quackStrategy = quackStrategy;
        this.flyStrategy = flyStrategy;
    }

    @Override
    public void display() {
        System.out.println("Hi! I'm a city duck!");
    }
}
