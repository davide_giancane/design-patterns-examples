package comportamentali.strategy;

public class WildDuck extends Duck {

    public WildDuck(QuackStrategy quackStrategy, FlyStrategy flyStrategy) {
        this.quackStrategy = quackStrategy;
        this.flyStrategy = flyStrategy;
    }

    @Override
    public void display() {
        System.out.println("Hi! I'm a wild duck!");
    }
}
