package comportamentali.strategy;

public class SimpleFlyStrategy implements FlyStrategy {
    @Override
    public void fly() {
        System.out.println("I'm simply flying...");
    }
}
