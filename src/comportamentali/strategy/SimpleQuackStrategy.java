package comportamentali.strategy;

public class SimpleQuackStrategy implements QuackStrategy {
    @Override
    public void quack() {
        System.out.println("simply quacking...QUA QUA!");
    }
}
