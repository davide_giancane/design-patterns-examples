package comportamentali.strategy;

public interface QuackStrategy {
    void quack();
}
