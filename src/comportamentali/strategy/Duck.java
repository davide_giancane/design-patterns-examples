package comportamentali.strategy;

public abstract class Duck {
    protected QuackStrategy quackStrategy;
    protected FlyStrategy flyStrategy;

    public void fly(){
        this.flyStrategy.fly();
    }

    public void quack(){
        this.quackStrategy.quack();
    }

    public abstract void display();
}
