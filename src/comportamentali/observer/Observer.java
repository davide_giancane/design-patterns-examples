package comportamentali.observer;

public interface Observer {
    void update();
}
