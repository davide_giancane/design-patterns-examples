package comportamentali.observer;

public abstract class Display {
    protected String displayID;

    @Override
    public boolean equals(Object obj) {
        try {
            Display display = (Display) obj;
            boolean areEquals = false;
            if(displayID.equals(display.getDisplayID()))
                areEquals = true;
            return areEquals;
        } catch (ClassCastException e){
            return false;
        }
    }

    private String getDisplayID(){
        return displayID;
    }
}
