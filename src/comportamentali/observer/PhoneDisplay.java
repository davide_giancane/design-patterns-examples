package comportamentali.observer;

public class PhoneDisplay implements Observer {
    private Observable observable;
    private String phoneID;

    public PhoneDisplay(String phoneID, Observable station){
        this.phoneID = phoneID;
        observable = station;
    }

    @Override
    public void update() {
        System.out.println("PhoneDisplay: new temperature -> " + observable.getState());
    }
}
