package comportamentali.observer;

public class LCDDisplay extends Display implements Observer{
    private Observable observable;

    public LCDDisplay(String displayID, Observable station){
        this.displayID = displayID;
        this.observable = station;
    }

    @Override
    public void update() {
        System.out.println("LCDDisplay: new temperature -> " + observable.getState());
    }
}
