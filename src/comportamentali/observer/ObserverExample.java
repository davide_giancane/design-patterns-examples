package comportamentali.observer;

public class ObserverExample {
    public static void main(String[] args) {
        //creo observable e observers
        WeatherStation station = new WeatherStation();
        PhoneDisplay phoneDisplay = new PhoneDisplay("0001", station);
        LCDDisplay lcdDisplay  = new LCDDisplay("0002", station);

        //aggiungo gli observer all'observable
        station.addObserver(phoneDisplay);
        station.addObserver(lcdDisplay);

        station.changeRecordedTemperature(30.3);
        station.changeRecordedTemperature(28.5);

    }
}
