package comportamentali.observer;

import java.util.ArrayList;
import java.util.List;

/**
 * Ruolo: Concrete Observablen
 * In pratica è un termometro o più in generale può essere un sensore
 */
public class WeatherStation implements Observable {
    private List<Observer> observers; //lista contente gli observer da notificare
    private double recordedTemperature;

    public WeatherStation(){
        observers = new ArrayList<>();
        recordedTemperature = 0.0;
    }

    public void changeRecordedTemperature(double tmp){
        recordedTemperature = tmp;
        notifyObservers();
    }

    @Override
    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        observers.removeIf(obs -> {
            boolean finded = false;
            if(obs.equals(observer))
                finded = true;
            return finded;
        });
    }

    @Override
    public void notifyObservers() {
        for(Observer obs : observers){
            obs.update();
        }
    }

    @Override
    public Object getState() {
        return recordedTemperature;
    }
}
