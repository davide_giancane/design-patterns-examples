package comportamentali.state;

/**
 * Classe che implementa un cancello.
 * Possiede metodi per entrare, pagare con successo e con fallimento.
 * (Per ipotesi supponiamo che i metodi di pagamento siano richiamati da un thread secondario che si occupa di
 * pagare e verificare il pagamento eseguito. Sarà lui a richiamare uno di questi due metodi)
 */
public class Gate {
    private GateState state;

    //tecnicamente lo stato iniziale deve essere quello chiuso
    public Gate(){
        state = new ClosedState(this);
    }

    public void enter(){
        System.out.println("Trying to enter...");
        state.enter();
    }

    public void payOK(){
        System.out.println("Trying to pay...");
        state.payOK();
    }

    public void payFailed(){
        System.out.println("Trying to pay...");
        state.payFailed();
    }

    public void changeState(GateState newState){
        state = newState;
    }
}
