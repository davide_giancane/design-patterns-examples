package comportamentali.state;

/**
 * Stato in cui il cancello è chiuso
 */
public class OpenState implements GateState {
    private Gate gate;

    public OpenState(Gate gate){
        this.gate = gate;
    }

    @Override
    public void enter() {
        System.out.println("ENTERED! The gate is now closed!");
        gate.changeState(new ClosedState(gate));
    }

    @Override
    public void payOK() {
        System.out.println("You successfully payed again! Enter please...");
    }

    @Override
    public void payFailed() {
        System.out.println("PAYMENT FAILED: but you can still enter!");
    }
}
