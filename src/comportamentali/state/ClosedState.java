package comportamentali.state;

/**
 * Stato in cui in cancello è aperto
 */
public class ClosedState implements GateState {
    private Gate gate;

    public ClosedState(Gate gate){
        this.gate = gate;
    }

    @Override
    public void enter() {
        System.err.println("ALERT: the gate is closed, pay first!");
        //non cambio lo stato perchè chiuso era e chiuso rimane
    }

    @Override
    public void payOK() {
        System.out.println("Payment OK: the gate is open");
        gate.changeState(new OpenState(gate));
    }

    @Override
    public void payFailed() {
        System.err.println("ALERT: payment failed, the gate stays closed!");
    }
}
