package comportamentali.state;

public class StateExample {
    public static void main(String[] args) {
        Gate gate = new Gate();

        gate.enter(); //non riesce ad entrare perchè chiuso
        gate.payOK(); //il gate si apre
        gate.payOK(); //il gate resta aperto
        gate.enter(); //questa volta riesce ad entrare -> si chiude il gate
        gate.payFailed(); //il gate resta chiuso
    }
}
