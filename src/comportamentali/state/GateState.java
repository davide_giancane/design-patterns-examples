package comportamentali.state;

/**
 * Generica interfaccia per gli stati del sistema
 */
public interface GateState {
    void enter();
    void payOK();
    void payFailed();
}
