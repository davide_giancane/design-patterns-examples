package comportamentali.chain_of_responsability;

public class ChainPatternDemo {
    /**
     * Creo differenti tipi di loggers. Assegno loro i livelli e imposto per
     * ciascuno il successore nella catena.
     * @param args
     */
    public static void main(String[] args) {
        AbstractLogger loggerChain = getChainOfLoggers();

        loggerChain.logMessage(AbstractLogger.INFO, "This is an information");
        loggerChain.logMessage(AbstractLogger.DEBUG, "This is a debug information");
        loggerChain.logMessage(AbstractLogger.ERROR, "This is an error inforation");

    }

    public static AbstractLogger getChainOfLoggers(){
        AbstractLogger errorLogger = new ErrorLogger(AbstractLogger.ERROR);
        AbstractLogger fileLogger = new FileLogger(AbstractLogger.DEBUG);
        AbstractLogger consoleLogger = new ConsoleLogger(AbstractLogger.INFO);

        errorLogger.setNextLogger(fileLogger);
        fileLogger.setNextLogger(consoleLogger);

        return errorLogger;
    }
}
