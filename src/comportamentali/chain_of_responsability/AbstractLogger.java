package comportamentali.chain_of_responsability;

public abstract class AbstractLogger {
    public static int INFO = 1;
    public static int DEBUG = 2;
    public static int ERROR = 3;
    
    protected int level;
    
    //elemento successivo nella catena
    protected AbstractLogger nextLogger;
    
    public void setNextLogger(AbstractLogger nextLogger){
        this.nextLogger = nextLogger;
    }

    public void logMessage(int level, String message){
        if(this.level <= level)
            write(message);
        else{
            try{
                nextLogger.logMessage(level, message);
            } catch (NullPointerException e) {
                System.err.println("Richiesta non portata a termine!");
            }
        }
    }

    protected abstract void write(String message);
}
