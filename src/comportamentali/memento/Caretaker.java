package comportamentali.memento;

import java.util.*;


public class Caretaker {
    //Il Caretaker tiene traccia di tutti gli stati dell'originator in uno stack
    private Stack<Memento> mementos = new Stack<>();

    public void addMemento(Memento m){
        mementos.add(m);
    }

    public Memento getMemento(){
        return mementos.pop();
    }
}
