package comportamentali.memento;

public class Originator {
    private String state;
    /*
    Supponiamo che ci siano anche altri dati privati non necessari a definire
    lo stato e che non dovrebbero quindi essere salvati.
     */

    public void setState(String state){
        System.out.println("Originator: Setting state to " + state);
        this.state = state;
    }

    public Memento save(){
        System.out.println("Originator: Creating a Memento");
        return new Memento(state);
    }

    public void restore(Memento m){
        state = m.getState();
        System.out.println("Originator: state after restoring from Memento: " + state);
    }
}
