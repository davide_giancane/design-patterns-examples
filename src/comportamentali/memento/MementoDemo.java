package comportamentali.memento;

public class MementoDemo {
    public static void main(String[] args) {
        //creo gli attori del pattern
        Caretaker caretaker = new Caretaker();
        Originator originator = new Originator();

        //imposto due stati su originator sequenzialmente
        originator.setState("State 1");
        originator.setState("State 2");

        //salvo l'ultimo stato registrato (State 2)
        caretaker.addMemento(originator.save());
        originator.setState("State 3"); //e cambio nuovamente lo stato
        caretaker.addMemento(originator.save()); //salvando anche quest'ultimo
        originator.setState("State 4"); //cambio ancora una volta lo stato

        //ripristino l'ultimo stato salvato (State 3)
        originator.restore(caretaker.getMemento());
        //ripristino lo stato ancora precedente (State 2)
        originator.restore(caretaker.getMemento());
    }
}
