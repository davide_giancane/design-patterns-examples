package comportamentali.command;

/**
 * Ruolo: RECEIVER
 */
public class Light {
    private Color color, previousOne; //previousOne serve per l'undo
    private boolean on;

    /**
     * Costruisce l'oggetto lampadina. Di default il colore è WHITE
     */
    public Light(){
        this.color = Color.WHITE;
        this.previousOne = Color.WHITE;
        this.on = false;
    }

    /**
     * Accende la lampadina
     * @throws Exception se la lampadina è gia accesa
     */
    public void turnOn() throws Exception {
        if(on == true)
            throw new Exception("The light is already on");
        on = true;
    }

    /**
     * Spegne la lampadina
     * @throws Exception se la lampadina è già spenta
     */
    public void turnOff() throws Exception{
        if(on == false)
            throw new Exception("The light is already off");
        on = false;
    }

    /**
     * Cambia il colore della lampadina
     * @param color nuovo colore da impostare o null se si vuole ripristinare quello precedente
     * @throws Exception se il nuovo colore da impostare coincide con quello attuale
     */
    public void changeColor(Color color) throws Exception {
        if(on == false)
            throw new Exception("The light is off: turne it on first!");
        else {
            if (this.color.equals(color))
                throw new Exception("The light is already " + color);
            else if (color == null)
                this.color = previousOne;
            else {
                previousOne = this.color;  //salvo il valore del colore precedente e poi lo cambio
                this.color = color;
            }
        }
    }
}
