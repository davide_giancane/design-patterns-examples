package comportamentali.command;

public class ChangeLightColorCommand implements Command {
    private Light light;
    private Color color;

    public ChangeLightColorCommand(Light light, Color color){
        this.light = light;
        this.color = color;
    }

    @Override
    public void execute() {
        try {
            light.changeColor(color);
            System.out.println("The light color has become " + color);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    @Override
    public void unexecute() {
        try {
            light.changeColor(null);
            System.out.println("The light was restored to the previous color");
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}
