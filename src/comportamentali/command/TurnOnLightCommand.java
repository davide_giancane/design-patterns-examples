package comportamentali.command;

public class TurnOnLightCommand implements Command {
    private Light light;

    public TurnOnLightCommand(Light light){
        this.light = light;
    }

    @Override
    public void execute() {
        try {
            light.turnOn();
            System.out.println("The light has turned ON");
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    @Override
    public void unexecute() {
        try {
            light.turnOff();
            System.out.println("The light has turned off back");
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}
