package comportamentali.command;

public class CommandExample {
    public static void main(String[] args) {
        //creo il receiver
        Light light = new Light();

        Command turnOn = new TurnOnLightCommand(light);
        Command turnOff = new TurnOffLightCommand(light);
        Command changeColor = new ChangeLightColorCommand(light, Color.GREEN);
        Command changeColor2 = new ChangeLightColorCommand(light, Color.YELLOW);

        RemoteController controller = new RemoteController(turnOn, turnOff, changeColor, changeColor2);
        controller.on(); //accendo
        controller.up(); //cambio primo colore
        controller.down(); //cambio secondo colore
        controller.off(); //spengo
    }
}
