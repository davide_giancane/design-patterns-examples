package comportamentali.command;

public class TurnOffLightCommand implements Command {
    private Light light;

    public TurnOffLightCommand(Light light){
        this.light = light;
    }

    @Override
    public void execute() {
        try {
            light.turnOff();
            System.out.println("The light has turned OFF");
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    @Override
    public void unexecute() {
        try {
            light.turnOn();
            System.out.println("The light has turned on back");
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}
