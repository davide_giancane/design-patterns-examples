package comportamentali.command;

public interface Command {
    public void execute();
    public void unexecute();
}
