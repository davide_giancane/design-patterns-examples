package comportamentali.command;

/**
 * Ruolo: INVOKER
 */
public class RemoteController {
    private Command onButton, offButton, upButton, downButton;

    public RemoteController(Command on, Command off, Command up, Command down) {
        onButton = on;
        offButton = off;
        upButton = up;
        downButton = down;
    }

    public void on(){
        onButton.execute();
    }

    public void off(){
        offButton.execute();
    }

    public void up(){
        upButton.execute();
    }

    public void down(){
        downButton.execute();
    }

}
