package comportamentali.visitor;

/**
 * Interfaccia comune per tutti i tipi di parti del computer
 */
public interface ComputerPart {
    //Metodo per accettare un visitor
    public void accept(ComputerPartVisitor computerPartVisitor);
}
