package strutturali.flyweight;

public class Car implements Vehicle {
    private String engine;
    private Color color;

    public Car(String engine, Color color) {
        this.engine = engine;
        this.color = color;
    }

    @Override
    public void start(String road) {
        System.out.println(color + " car with engine " + engine + " started in road " + road);
    }

    @Override
    public void stop() {
        System.out.println(color + " car with engine " + engine + " stopped!");
    }

    @Override
    public Color getColor() {
        return color;
    }
}
