package strutturali.flyweight;

import java.util.HashMap;
import java.util.Map;

public class VehicleFactory {
    private static Map<Color, Vehicle> vehiclesCache = new HashMap<>();

    public static Vehicle createVehicle(Color color){
        Vehicle newVehicle = vehiclesCache.computeIfAbsent(color, newColor -> {
            String newEngine = "V8";
            System.out.println("Sto creando un nuovo veicolo di colore " + newColor + " ...");
            return new Car(newEngine, newColor);
        });
        return newVehicle;
    }
}
