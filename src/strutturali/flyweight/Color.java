package strutturali.flyweight;

public enum Color {
    BLUE, BLACK, WHITE, RED, YELLOW, GREEN
}
