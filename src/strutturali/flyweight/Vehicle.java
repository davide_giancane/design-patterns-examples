package strutturali.flyweight;

public interface Vehicle {
    void start(String road); //stato esterno, dipendente dal contesto
    void stop();
    public Color getColor();
}
