package strutturali.flyweight;

public class FlyweightExample {
    public static void main(String[] args) {

        //chiamata dove l'oggetto flyweight viene costruito
        Vehicle car = VehicleFactory.createVehicle(Color.BLUE);
        car.start("via comelico");
        System.out.println("car - colore: " + car.getColor() + "\n");

        //anche qui un nuovo oggetto flyweight viene costruito
        Vehicle car2 = VehicleFactory.createVehicle(Color.YELLOW);
        car2.start("piazza duomo");
        System.out.println("car2 - colore: " + car2.getColor() + "\n");

        //Qui l'oggetto non verrà costruito, dato esiste già una macchina
        //di colore blu. La factory si limiterà quindi solitamente a restituirlo
        Vehicle alreadyBuiltCar = VehicleFactory.createVehicle(Color.BLUE);
        alreadyBuiltCar.start("via gioia");
        System.out.println("alreadyBuiltCar - color: " + alreadyBuiltCar.getColor());
    }
}
