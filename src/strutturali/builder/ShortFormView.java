package strutturali.builder;

/**
 * Ruolo: REFINED ABSTRACTION N.2
 * Classe che vuole rappresentare un form minimale con due informazioni sulla risorsa
 */
public class ShortFormView extends View {

    protected ShortFormView(IResource r) {
        super(r);
    }

    @Override
    public String show() {
        return ("*** SHORT FORM VIEW ***" + "\n" +
                "Title: " + resource.title() + "\n" +
                "Snippet: " + resource.snippet()
        );
    }
}
