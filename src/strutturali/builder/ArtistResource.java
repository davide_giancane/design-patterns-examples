package strutturali.builder;

/**
 * Ruolo: CONCRETE IMPLEMENTOR N.1
 *
 * Nell'esempio agisce anche da adapter verso la classe artist. Infatti possiede un'istanza della classe artist al
 * fine di applicare il pattern e rendere compatibile Artist con View
 */
public class ArtistResource implements IResource {
    private Artist artist;

    public ArtistResource(Artist artist){
        this.artist = artist;
    }

    @Override
    public String snippet() {
        return artist.getBiography();
    }

    @Override
    public String image() {
        return artist.getImage();
    }

    @Override
    public String title() {
        return artist.getName();
    }

    @Override
    public String url() {
        return artist.getWebSite();
    }
}
