package strutturali.builder;

public class Artist {
    private String bio, name, image, webSite;

    public Artist(String biography, String name, String image, String webSite){
        this.bio = biography;
        this.name = name;
        this.image = image;
        this.webSite = webSite;
    }
    public String getBiography() {
        return bio;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public String getWebSite(){
        return webSite;
    }
}
