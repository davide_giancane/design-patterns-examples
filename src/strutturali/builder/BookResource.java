package strutturali.builder;

import java.net.URL;

/**
 * Ruolo: CONCRETE IMPLEMENTOR N.2
 *
 * Nell'esempio agisce anche da adapter verso la classe Book, con lo scopo di rendere compatibile l'interfaccia View
 * con Book
 */
public class BookResource implements IResource {
    private Book book;

    public BookResource(Book book){
        this.book = book;
    }
    @Override
    public String snippet() {
        return book.getDescription();
    }

    @Override
    public String image() {
        return book.getCover();
    }

    @Override
    public String title() {
        return book.getTitle();
    }

    @Override
    public String url() {
        return "No URL Available";
    }
}
