package strutturali.builder;

import java.net.MalformedURLException;
import java.net.URL;

public class BuilderExample {
    public static void main(String[] args) {
        Book book = new Book("Design pattern", "The main book for learning Design Patterm", "GAMMA IMAGE URL");
        Artist artist = new Artist("The band was born in 2013 and....", "Overkhaos", "OK IMAGE URL", "www.overkhaos.com");

        IResource artistResource = new ArtistResource(artist);
        IResource bookResource = new BookResource(book);

        View longFormView1 = new LongFormView(artistResource);
        View longFormView2 = new LongFormView(artistResource);

        View shortFormView1 = new ShortFormView(artistResource);
        View shortFormView2 = new ShortFormView(bookResource);

        display(longFormView1.show());
        display(longFormView2.show());
        display(shortFormView1.show());
        display(shortFormView2.show());
    }

    private static void display(String content){
        System.out.println(content + "\n");
    }
}
