package strutturali.builder;

import java.net.URL;

/**
 * Interfaccia IMPLEMENTOR
 *
 */
public interface IResource {
    public String snippet();
    public String image(); //per praticitià la concepisco come una stringa
    public String title();
    public String url();
}
