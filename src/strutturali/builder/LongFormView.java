package strutturali.builder;

/**
 * Ruolo: REFINED ABSTRACTION N.1
 * Classe che vuole rappresentare un form lungo con molte informazioni da mostrare
 */
public class LongFormView extends View {

    public LongFormView(IResource r) {
        super(r);
    }

    @Override
    public String show() {
        return ("**** LONG FORM VIEW ****" + "\n" +
                "Title: " + resource.title() + "\n" +
                "Image: " + resource.image() + "\n" +
                "Snippet: " + resource.snippet() + "\n" +
                "Link: " + resource.url()
        );
    }
}
