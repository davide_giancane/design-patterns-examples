package strutturali.builder;

/**
 * Ruolo: ABSTRACTION
 * Classe astratta che simboleggia una generica schermata di un'applicazione. Esistono diverse tipologie di schermate
 * che saranno sottoclassi di View. Lo scopo del bridge pattern è quello di fare in modo che ogni tipologia di view
 * sia compatibile con ogni tipologia di risorsa (di classe IResource) che dovrà mostrare a video
 */
public abstract class View {
    //risorsa da controllare nella schermata
    protected IResource resource;

    public View(IResource r){
        this.resource = r;
    }

    /**
     * Mostra a schermo la particolare risolta associata.
     * Metodo da far implementare alla sottoclassi
     * (Possiamo pensare ad esempio che venga restituito sottoforma di contenuto html)
     * @return Stringa che rappresenta il contenuto della risorsa
     */
    public abstract String show();


}
