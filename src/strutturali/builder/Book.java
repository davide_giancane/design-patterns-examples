package strutturali.builder;

public class Book {
    private String title, description, cover;

    public Book(String title, String description, String cover){
        this.title = title;
        this.description = description;
        this.cover = cover;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getCover() {
        return cover;
    }
}
