In questo esempio si vuole implementare il Proxy Pattern per capire le entità che sono coinvolte e la dinamica.
In generale, un Proxy può essere applicato ad esempio quando nel nostro sistema è presente un oggetto che possiede
l'operazione di costruzione troppo complessa (questa come qualsiasi altro genere di operazione possibile).

In questo esempio supponiamo quindi di avere un oggetto RealSubject la cui costruzione è onerosa in termini
computazionali. Il pattern proxy viene qui impiegato per creare un sostituto di questo oggetto, che si occupi di
interagire col client al posto dell'oggetto vero per *ritardare* la sua costruzione.
(Una sorta di LAZY EVALUATION)

Cioè, anzichè fare in modo che il client crei esso stesso l'oggetto complesso senza magari utilizzarlo immediatamente,
comportando quindi una computazione onerosa al momento inutile, il client crea il suo Proxy (che chiaramente avrà la
stessa interfaccia dell'oggetto reale) il quale si occuperà di istanziare il Real Subject solo quando il client vorrà
usufruire di una certa operazione (nell'esempio è request()).

Chiamata questa operazione sul proxy, questo si occuperà di istanziare l'oggetto (auspicabilmente una sola volta) e
tenere memorizzato il suo riferimento per non doverlo creare nuovamente (meccanismo di caching).

Dal momento che il client interagisce con un oggetto che ha la stessa interfaccia di quello reale, l'utilizzo del
pattern è del tutto trasparente a lui.