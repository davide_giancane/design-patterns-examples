package strutturali.proxy;

public class RealSubject implements Subject{
    private String name;

    /**
     * Operazione di costruzione che supponiamo essere costosa da eseguire.
     * @param name nome del raeal subject
     */
    public RealSubject(String name){
        this.name = name;
        System.out.println("*** REAL SUBJCT COSTRUITO ***");
    }

    @Override
    public void request() {
        System.out.println("Richiesta richiamata sul Real Subject");
    }
}
