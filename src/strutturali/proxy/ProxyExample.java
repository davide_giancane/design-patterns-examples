package strutturali.proxy;

public class ProxyExample {
    public static void main(String[] args) {
        //istanzio il proxy (l'oggetto reale non viene creato)
        Subject proxy = new Proxy("ciao");
        //.......
        //.......   [codice intermedio fra creazione ed utilizzo dell'oggetto]
        //.......

        //momento di richiesta di una operazione --> il proxy crea l'oggetto e richiama la sua operazione
        proxy.request();
    }
}
