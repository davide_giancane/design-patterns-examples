package strutturali.proxy;

public interface Subject {
    public void request();
}
