package strutturali.proxy;

public class Proxy implements Subject{
    private Subject realSubject;
    private String realSubjectName;

    public Proxy(String realSubjectParameter){
        this.realSubjectName = realSubjectParameter;
    }

    @Override
    public void request() {
        if(realSubject == null)
            realSubject = new RealSubject(realSubjectName);
        System.out.println("Proxy delega la chiesta al real subject ... ");
        realSubject.request();
    }
}
