package strutturali.adapter;

public class Mp4Player implements AdvancedAudioPlayer{
    @Override
    public void playVlc(String fileName) {
        //do nogthing
    }

    @Override
    public void playmMp4(String fileName) {
        System.out.println("Playing mp4 file. Name: " + fileName);
    }
}
