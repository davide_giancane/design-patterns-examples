package strutturali.adapter;

public class MediaAdapter implements MediaPlayer {

    //USO DELLA COMPOSIZIONE DI OGGETTI: necessaria per rendere compatibili
    //le due interfacce
    private AdvancedAudioPlayer advancedAudioPlayer;

    public MediaAdapter(AudioType audioType){
        if(audioType.equals(AudioType.VLC))
            advancedAudioPlayer = new VlcPlayer();
        else if(audioType.equals(AudioType.MP4))
            advancedAudioPlayer = new Mp4Player();
    }
    @Override
    public void play(AudioType audioType, String fileName) {
        if(audioType.equals(AudioType.VLC))
            advancedAudioPlayer.playVlc(fileName);
        else if(audioType.equals(AudioType.MP4))
            advancedAudioPlayer.playmMp4(fileName);
    }
}
