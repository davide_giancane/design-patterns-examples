package strutturali.adapter;

public interface AdvancedAudioPlayer {
    public void playVlc(String fileName);
    public void playmMp4(String fileName);
}
