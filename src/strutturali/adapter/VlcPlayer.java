package strutturali.adapter;

public class VlcPlayer implements AdvancedAudioPlayer {
    @Override
    public void playVlc(String fileName) {
        System.out.println("Playing vlc file. Name: " + fileName);
    }

    @Override
    public void playmMp4(String fileName) {
        //do nothing
    }
}
