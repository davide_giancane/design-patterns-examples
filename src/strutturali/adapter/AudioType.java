package strutturali.adapter;

public enum AudioType {
    MP3, MP4, VLC
}
