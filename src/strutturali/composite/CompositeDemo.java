package strutturali.composite;

public class CompositeDemo {
    public static StringBuffer compositeBuilder = new StringBuffer();

    public static void main(String[] args) {
        Directory music = new Directory("Music");
        Directory dreamTheater = new Directory("Dream Theater");
        Directory metallica = new Directory("Metallica");

        File track1 = new File("Pull me under.mp3");
        File track2 = new File("Dance of Eternity.mp3");
        File track3 = new File("Back in Black.mp3");
        File track4 = new File("Master of Puppets.mp3");
        File track5 = new File("Enter Sandman.mp3");
        File track6 = new File("And Justice For All.mp3");

        music.add(dreamTheater);
        music.add(track3);
        music.add(metallica);

        dreamTheater.add(track1);
        dreamTheater.add(track2);
        metallica.add(track4);
        metallica.add(track5);
        metallica.add(track6);

        music.ls();
    }
}
