package strutturali.composite;

/**
 * Definisce un "minimo denominatore comune"
 * (Comando ls tipico dei sistemi LINUX)
 */
public interface AbstractFile {
    void ls();
}
