package strutturali.composite;

import java.util.ArrayList;
import java.util.List;

public class Directory implements AbstractFile {
    private String name;
    private List<AbstractFile> includedFiles;

    public Directory(String name){
        this.name = name;
        this.includedFiles = new ArrayList();
    }

    public void add(AbstractFile file){
        includedFiles.add(file);
    }

    @Override
    public void ls() {
        System.out.println(CompositeDemo.compositeBuilder + name.toUpperCase());
        CompositeDemo.compositeBuilder.append("   ");
        for(AbstractFile file : includedFiles){
            //riconduco i file al tipo generico per poi attuare la ricorsione
            //(Perchè possono anche essere altre directory)
            file.ls();
        }
        CompositeDemo.compositeBuilder.setLength(CompositeDemo.compositeBuilder.length() - 3);
    }
}
