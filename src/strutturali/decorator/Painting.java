package strutturali.decorator;

public class Painting implements VisualComponent {
    private String paintingDescription;

    public Painting(String paintingDescription){
        this.paintingDescription = paintingDescription;
    }

    @Override
    public void hang() {
        System.out.print(paintingDescription);
    }
}
