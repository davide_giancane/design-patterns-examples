package strutturali.decorator;

public interface VisualComponent {
    public void hang();
}
