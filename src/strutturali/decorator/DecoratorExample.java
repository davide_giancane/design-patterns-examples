package strutturali.decorator;

public class DecoratorExample {
    public static void main(String[] args) {

        //Costruzione dell'oggetto dall'esterno verso l'interno
        VisualComponent painting = new PaintingDecorator(
                new PaintingDecorator(
                        new Painting("Gioconda"), "Placca in oro"),
                "Cornice"
        );

        painting.hang();
    }

}
