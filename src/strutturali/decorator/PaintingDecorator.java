package strutturali.decorator;

/**
 * Classe decorator che può anche essere pensata come classe astratta, in modo
 * tale da poter definire tramite ereditarietà diversi tipi di decoratori
 */
public class PaintingDecorator implements VisualComponent {
    private VisualComponent component;
    private String decoratorDescription;

    public PaintingDecorator(VisualComponent component, String decoratorDescription){
        this.component = component;
        this.decoratorDescription = decoratorDescription;
    }

    @Override
    public void hang() {
        component.hang();
        System.out.print(" + " + decoratorDescription);
    }
}
