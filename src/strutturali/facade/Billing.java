package strutturali.facade;

public class Billing {
    private String product;

    public Billing(String product){
        this.product = product;
    }

    public void bil(){
        System.out.println("Billing the order for the product: " + product);
    }
}
