package strutturali.facade;

public class OrderFulfilment {
    private String product, orderID;

    public OrderFulfilment(String product, String orderID){
        this.product = product;
        this.orderID = orderID;
    }

    public void fulfil(){
        System.out.println("Fulfilling order " + orderID + "\n" +
                "Product: " + product
        );
    }
}
