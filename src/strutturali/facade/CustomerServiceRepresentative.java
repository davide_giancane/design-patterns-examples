package strutturali.facade;

/**
 * Facade: si interfaccia con le classi di business orderFulfilment, Shipping e Billing per conto del client fornendo
 * a quest'ultimo un'interfaccia chiara e di facile interazione per manipolare le 3 classi in questione
 */
public class CustomerServiceRepresentative {
    private OrderFulfilment orderFulfilment;
    private Shipping shipping;
    private Billing billing;
    private int orderID = 0000;

    public CustomerServiceRepresentative(String productName){
        this.orderFulfilment = new OrderFulfilment(productName, Integer.toString(orderID));
        this.shipping = new Shipping(productName);
        this.billing = new Billing(productName);
        orderID += 1;
    }

    public void fulfilOrder(){
        orderFulfilment.fulfil();
    }

    public void shipProduct(){
        shipping.ship();
    }

    public void bilProduct(){
        billing.bil();
    }
}
