package strutturali.facade;

public class Shipping {
    private String product;

    public Shipping(String product) {
        this.product = product;
    }

    public void ship(){
        System.out.println("Shipping " + product + " ...");
    }
}
