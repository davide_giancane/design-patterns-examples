package strutturali.facade;

/**
 * Il client che interagisce con il Facade CustomerServiceRepresentative
 */
public class Client {
    public static void main(String[] args) {
        CustomerServiceRepresentative representative = new CustomerServiceRepresentative("Chitarra elettrica");

        representative.fulfilOrder();
        representative.bilProduct();
        representative.shipProduct();
    }
}
