package creazionali.builder;

/**
 * CONCRETE BUILDER
 *
 * Implementa unicamente i metodi specifici. Metodi la cui implementazione
 * differisce da quella di altri builder concreti
 */
public class HawaiianPizzaBuilder extends PizzaBuilder {
    @Override
    public void buildDough() {
        this.pizza.setDough("cross");
    }

    @Override
    public void buildSauce() {
        this.pizza.setSauce("mild");
    }

    @Override
    public void buildTopping() {
        this.pizza.setTopping("ham + pineapple");
    }
}
