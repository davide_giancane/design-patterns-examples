package creazionali.builder;

/**
 * DIRECTOR
 *
 * Costruisce il Product (pizza) invocando i metodi dell'inferfaccia
 * del Builder Astratto
 */
public class Cook {
    private PizzaBuilder pizzaBuilder;

    public Cook(PizzaBuilder pb){
        pizzaBuilder = pb;
    }

    public Pizza getPizza(){
        return pizzaBuilder.getPizza();
    }

    public void constructPizza(){
        pizzaBuilder.createNewPizzaProduct();
        pizzaBuilder.buildDough();
        pizzaBuilder.buildSauce();
        pizzaBuilder.buildTopping();
    }
}
