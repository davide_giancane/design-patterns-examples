package creazionali.builder;

/**
 * ABSTRACT BUILDER
 *
 * Fornisce una implementazione di default per i metodi che sono comuni a tutte
 * le tipologie di builder concreti, nella fattispecie l'istanziazione di una
 * nuova pizza e il suo ritorno al chiamante. I metodi che ciascun builder concreto
 * dovrà implementare sono quelli relativi ai PASSI di costruzione, che sono
 * dichiarati astratti
 */
public abstract class PizzaBuilder {
    protected Pizza pizza;

    public Pizza getPizza(){return pizza;}

    public void createNewPizzaProduct(){
        pizza = new Pizza();
    }

    public abstract void buildDough();
    public abstract void buildSauce();
    public abstract void buildTopping();
}
