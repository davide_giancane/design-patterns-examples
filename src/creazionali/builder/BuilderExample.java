package creazionali.builder;

/**
 * Costruzione di un dato tipo di pizza
 */
public class BuilderExample {
    public static void main(String[] args) {
        Cook cook1 = new Cook(new HawaiianPizzaBuilder());
        Cook cook2 = new Cook(new SpicyPizzaBuilder());

        cook1.constructPizza();
        cook2.constructPizza();

        Pizza pizza1 = cook1.getPizza();
        Pizza pizza2 = cook2.getPizza();

        System.out.println("Pizza type 1" + ": " + pizza1.toString());
        System.out.println("Pizza type 2" + ": " + pizza2.toString());
    }
}
