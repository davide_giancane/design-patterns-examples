package creazionali.abstract_factory;

interface VehicleFactory {
    public Car createCar(String model, char[] licensePlate, double maxSpeed);
    public Motorbike createMotorbike(String model, char[] chassis, double maxSpeed);
}
