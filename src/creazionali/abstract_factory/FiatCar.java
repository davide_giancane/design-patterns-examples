package creazionali.abstract_factory;

public class FiatCar extends Car{
    FiatCar(String model, char[] licencePlate, double maxSpeed){
        this.brand = "Fiat";
        this.model = model;
        this.licensePlate = licencePlate;
        this.maxSpeed = maxSpeed;
    }
    @Override
    String carDescription() {
        return ("Auto: " + this.brand + " " + this.model);
    }

    @Override
    double getMaxSpeed() {
        return this.maxSpeed;
    }

    @Override
    char[] getLicensePlate() {
        return this.licensePlate;
    }
}
