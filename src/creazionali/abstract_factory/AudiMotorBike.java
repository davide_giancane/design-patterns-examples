package creazionali.abstract_factory;

public class AudiMotorBike extends Motorbike {
    AudiMotorBike(String model, char[] chassis, double maxSpeed){
        this.brand = "Audi";
        this.model = model;
        this.chassis = chassis;
        this.maxSpeed = maxSpeed;
    }
    @Override
    String motorbikeDescription() {
        return ("Moto: " + this.brand + " " + this.model);
    }

    @Override
    char[] getChassisNumber() {
        return this.chassis;
    }

    @Override
    double getMaxSpeed() {
        return this.maxSpeed;
    }
}
