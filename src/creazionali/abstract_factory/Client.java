package creazionali.abstract_factory;

class Client {
    public static void main(String[] args) {
        //Costruzione delle factory
        VehicleFactory audiFactory = new AudiFactory();
        VehicleFactory fiatFactory = new FiatFactory();

        //Creazione degli oggetti
        char[] licensePlate = {'E','S','2','1','5','Y','L'};
        Car audiCar = audiFactory.createCar("A3-sportback", licensePlate, 240);

        char[] chassis = {'t','a','4','6','r'};
        Motorbike fiatBike = fiatFactory.createMotorbike("Razzo", chassis, 201.9);

        System.out.println(audiCar.carDescription());
        System.out.println(fiatBike.motorbikeDescription());
    }
}
