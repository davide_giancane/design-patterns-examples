package creazionali.abstract_factory;

import java.util.ArrayList;
import java.util.List;

public class FiatMotorbike extends Motorbike {
    FiatMotorbike(String model, char[] chassis, double maxSpeed){
        this.brand = "Fiat";
        this.model = model;
        this.chassis = chassis;
        this.maxSpeed = maxSpeed;
    }
    @Override
    String motorbikeDescription() {
        return ("Moto: " + this.brand + " " + this.model);
    }

    @Override
    char[] getChassisNumber() {
        List list = new ArrayList();
        return this.chassis;
    }

    @Override
    double getMaxSpeed() {
        return this.maxSpeed;
    }
}
