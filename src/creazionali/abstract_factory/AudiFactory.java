package creazionali.abstract_factory;

class AudiFactory implements VehicleFactory {
    @Override
    public Car createCar(String model, char[] licensePlate, double maxSpeed) {
        return new AudiCar(model, licensePlate, maxSpeed);
    }

    @Override
    public Motorbike createMotorbike(String model, char[] chassis, double maxSpeed) {
        return new AudiMotorBike(model, chassis, maxSpeed);
    }
}
