package creazionali.abstract_factory;

class FiatFactory implements VehicleFactory {
    @Override
    public Car createCar(String model, char[] licensePlate, double maxSpeed) {
        return new FiatCar(model, licensePlate, maxSpeed);
    }

    @Override
    public Motorbike createMotorbike(String model, char[] chassis, double maxSpeed) {
        return new FiatMotorbike(model, chassis, maxSpeed);
    }
}
