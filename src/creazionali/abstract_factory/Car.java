package creazionali.abstract_factory;

abstract class Car {
    protected String brand;
    protected String model;
    protected double maxSpeed;
    protected char[] licensePlate = new char[7];

    abstract String carDescription();
    abstract double getMaxSpeed();
    abstract char[] getLicensePlate();
}
