package creazionali.abstract_factory;

abstract class Motorbike {
    protected String brand;
    protected String model;
    protected char[] chassis = new char[5];
    protected double maxSpeed;

    abstract String motorbikeDescription();
    abstract char[] getChassisNumber();
    abstract double getMaxSpeed();
}
