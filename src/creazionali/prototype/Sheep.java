package creazionali.prototype;

public class Sheep extends Animal {
    public Sheep(){
        System.out.println("Sheep is Made");
    }
    @Override
    public Animal makeCopy() throws CloneNotSupportedException {
        System.out.println("Sheep is being made");
        return super.makeCopy();
    }

    @Override
    public String toString() {
        return "Dolly is my Hero, Baaaa";
    }
}
