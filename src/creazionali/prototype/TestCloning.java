package creazionali.prototype;

public class TestCloning {
    public static void main(String[] args) {
        Animal sally = new Sheep();

        Animal clonedSheep = null;
        try {
            clonedSheep = sally.makeCopy();
            System.out.println(sally);
            System.out.println(clonedSheep);
            System.out.println("Sally Hashcode: " + System.identityHashCode(System.identityHashCode(sally)));
            System.out.println("Clone Hashcode: " + System.identityHashCode(System.identityHashCode(clonedSheep)));
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }
}
