package creazionali.prototype;

public abstract class Animal implements Cloneable {
    public Animal makeCopy() throws CloneNotSupportedException {
        Cloneable theClone = (Cloneable) super.clone();
        return (Animal) theClone;
    }
}
