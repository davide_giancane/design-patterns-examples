package creazionali.factory_method;

enum PizzaType {
    CHEESE, PEPPERONI;
}
