package creazionali.factory_method;

abstract class PizzaStore {
    //factory method
    abstract Pizza createPizza(PizzaType item);

    Pizza orderPizza(PizzaType type){
        Pizza pizza = createPizza(type);
        System.out.println("--- Making a " + pizza.getName() + " ---");
        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();
        return pizza;
    }
}
