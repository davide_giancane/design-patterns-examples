package creazionali.factory_method;

class ChicagoStyleCheesePizza extends Pizza {
    ChicagoStyleCheesePizza(){
        name = "Chicago Style Cheese Pizza";
        dough = "Extra Thick Crust Dough";
        sauce = "Plum Totato Sauce";

        toppings.add("Shredded Mozzarella Cheese");
        toppings.add("Sliced Parmigiano Reggiano");
        toppings.add("Crescenza Cheese");
        toppings.add("Eggplant");
        toppings.add("Stracchino Cheese");
    }
}
