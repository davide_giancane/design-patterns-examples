package creazionali.factory_method;

class PizzaTest {
    public static void main(String[] args) {
        //Ordering a pizza from chicago
        PizzaStore pizzaStore = new ChicagoPizzaStore();
        Pizza chicagoPizza = pizzaStore.orderPizza(PizzaType.CHEESE);

        //Ordering a pizza from NY
        pizzaStore = new NYPizzaStore();
        Pizza nyPizza = pizzaStore.orderPizza(PizzaType.PEPPERONI);

    }
}
