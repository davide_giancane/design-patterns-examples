package creazionali.factory_method;

class ChicagoStylePepperoniPizza extends Pizza {
    ChicagoStylePepperoniPizza(){
        name = "Chicago Style Pepperoni Pizza";
        dough = "Extra Thick Crust Dough";
        sauce = "Plum Totato Sauce";

        toppings.add("Shredded Mozzarella Cheese");
        toppings.add("Black Olives");
        toppings.add("Spinach");
        toppings.add("Eggplant");
        toppings.add("Scliced Pepperoni");
    }

    @Override
    void cut() {
        System.out.println("Cutting the pizza into square slices");
    }
}
