package creazionali.factory_method;

import java.util.ArrayList;
import java.util.List;

abstract class Pizza {
    String name, dough, sauce;
    List<String> toppings = new ArrayList<>();

    String getName() {
        return name;
    }

    void prepare(){
        System.out.println("Preparing " + name);
    }

    void bake(){
        System.out.println("Baking " + name);
    }

    void cut(){
        System.out.println("Cutting " + name);
    }

    void box(){
        System.out.println("Boxing " + name);
    }
}
