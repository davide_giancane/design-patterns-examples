package creazionali.factory_method;

class ChicagoPizzaStore extends PizzaStore{
    @Override
    Pizza createPizza(PizzaType item) {
        switch (item) {
            case CHEESE:
                return new ChicagoStyleCheesePizza();
            case PEPPERONI:
                return new ChicagoStylePepperoniPizza();
            default:
                return null;
        }
    }
}
