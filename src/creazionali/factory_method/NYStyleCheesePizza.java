package creazionali.factory_method;

class NYStyleCheesePizza extends Pizza {
    NYStyleCheesePizza() {

        name = "NY Style Cheese Pizza";
        dough = "Thin Crust Dough";
        sauce = "Marinara Sauce";

        toppings.add("Grated Reggiano Cheese");
        toppings.add("Ricotta Cheese");
        toppings.add("Rocket Salad");
        toppings.add("Onion");
        toppings.add("Junket");
    }
}
