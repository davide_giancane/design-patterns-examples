package creazionali.factory_method;

class NYPizzaStore extends PizzaStore {
    @Override
    Pizza createPizza(PizzaType item) {
        switch (item) {
            case CHEESE:
                return new NYStyleCheesePizza();
            case PEPPERONI:
                return new NYStylePepperoniPizza();
            default:
                return null;
        }
    }
}
