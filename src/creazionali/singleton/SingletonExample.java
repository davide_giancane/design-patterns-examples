package creazionali.singleton;

public class SingletonExample {
    public static void main(String[] args) {
        //prima istanza richiesta --> messaggio di istanza creata a video
        Singleton singleton1 = Singleton.getInstance();

        //istanza richiesta nuovamente --> messaggio di istanza non creata ma reperita
        Singleton singleton2 = Singleton.getInstance();
    }
}
