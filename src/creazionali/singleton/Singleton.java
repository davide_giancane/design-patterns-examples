package creazionali.singleton;

public class Singleton {
    private static Singleton instance;

    private Singleton(){}

    public static Singleton getInstance(){
        print("Ricavo il singleton...");
        if(instance == null){
            instance = new Singleton();
            print("ISTANZA CREATA");
        } else
            print("Istanza già creata, la ritorno!");
        return instance;
    }

    private static void print(String msg){
        System.out.println(msg);
    }
}
